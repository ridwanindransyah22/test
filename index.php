<?php
	$koneksi=mysqli_connect("localhost","root","","perpus") or die("Gagal Koneksi Database");
	echo "Sukses Koneksi database";
?>

<html>
	<head>
		<title> Database Perpustakaan </title>
	</head>

	<body background="bg1.gif">
	<center>
		<legend align="center"><font face="orator std" color="lightblue" size="6"><b> Tabel Anggota </b></font></legend>
		<table border="1" width="1000" height="200">
			<tr bgcolor="dodgerblue" height="35">
				<th> ID Anggota </th>
				<th> Nama </th>
				<th> Jenis Kelamin </th>
				<th> Alamat </th>
				<th> No. HP </th>
				<th> Email </th>
				<th> Tanggal Entry </th>
				<th colspan="2"> Action </th>
			</tr>

			<?php
			$query = "select * from anggota";
			$data = mysqli_query($koneksi,$query) or die("Gagal Query:".$query);
			?>

			<?php while($v=mysqli_fetch_array($data)):;
			?>

			<tr height="35" bgcolor="lightblue" align="center">
				<td> <?php echo $v["ID_Anggota"];?> </td>
				<td> <?php echo $v["Nama"];?></td>
				<td> <?php echo $v["Jenis_Kelamin"];?></td>
				<td> <?php echo $v["Alamat"];?> </td>
				<td> <?php echo $v["No_HP"];?> </td>
				<td> <?php echo $v["Email"];?> </td>
				<td> <?php echo $v["Tgl_Entry"];?> </td>
				<td><a href="aksi_hapus_anggota.php?ID_Anggota=<?php echo $v["ID_Anggota"];?>"> Hapus </a></td>
				<td><a href="formupdate_anggota.php?ID_Anggota=<?php echo $v["ID_Anggota"];?>"> Ubah </a></td>
			</tr>
		<?php endwhile;?>
		</table>
		<a href="input_data_anggota.php"> Masukkan Data </a><br><br><br>

		<legend align="center"><font face="orator std" color="lightblue" size="6"><b> Tabel Jenis Buku </b></font></legend>
		<table border="1" width="1000" height="200">
			<tr bgcolor="dodgerblue" height="25">
				<th> Kode Jenis </th>
				<th>Jenis Buku </th>
				<th colspan="2"> Action </th>
			</tr>

			<?php
			$query = "select * from jenis_buku";
			$data = mysqli_query($koneksi,$query) or die("Gagal Query:".$query);
			?>

			<?php while($v=mysqli_fetch_array($data)):;
			?>

			<tr height="25" bgcolor="lightblue" align="center">
				<td> <?php echo $v["kd_jenis"];?> </td>
				<td> <?php echo $v["jenis_buku"];?></td>
				<td><a href="aksi_hapus_jenis_buku.php?kd_jenis=<?php echo $v["kd_jenis"];?>"> Hapus </a></td>
				<td><a href="formupdate_jenis_buku.php?kd_jenis=<?php echo $v["kd_jenis"];?>"> Ubah </a></td>
			</tr>
		<?php endwhile;?>
		</table>
		<a href="input_data_jenis_buku.php"> Masukkan Data </a><br><br><br>

		<legend align="center"><font face="orator std" color="lightblue" size="6"><b> Tabel Buku </b></font></legend>
		<table border="1" width="1000" height="200">
			<tr bgcolor="dodgerblue" height="25">
				<th> Kode Buku </th>
				<th> Kode Jenis Buku </th>
				<th> Judul Buku </th>
				<th> Deskripsi </th>
				<th> Penulis </th>
				<th> Penerbit </th>
				<th> Harga </th>
				<th> Stok </th>
				<th colspan="2"> Action </th>
			</tr>

			<?php
			$query = "select * from buku";
			$data = mysqli_query($koneksi,$query) or die("Gagal Query:".$query);
			?>

			<?php while($v=mysqli_fetch_array($data)):;
			?>

			<tr height="75" bgcolor="lightblue" align="center">
				<td> <?php echo $v["kd_buku"];?> </td>
				<td> <?php echo $v["kd_jenis"];?></td>
				<td> <?php echo $v["judul_buku"];?></td>
				<td> <?php echo $v["description"];?> </td>
				<td> <?php echo $v["penulis"];?> </td>
				<td> <?php echo $v["penerbit"];?> </td>
				<td> <?php echo $v["harga"];?> </td>
				<td> <?php echo $v["stok"];?> </td>
				<td><a href="aksi_hapus_buku.php?kd_buku=<?php echo $v["kd_buku"];?>"> Hapus </a></td>
				<td><a href="formupdate_buku.php?kd_buku=<?php echo $v["kd_buku"];?>"> Ubah </a></td>
			</tr>
		<?php endwhile;?>
		</table>
		<a href="input_data_buku.php"> Masukkan Data </a><br><br><br>

		<legend align="center"><font face="orator std" color="lightblue" size="6"><b> Tabel Transaksi </b></font></legend>
		<table border="1" width="1000" height="200">
			<tr bgcolor="dodgerblue">
				<th> Kode Transaksi </th>
				<th> Kode Buku </th>
				<th> ID Anggota </th>
				<th> Tanggal Transaksi </th>
				<th colspan="2"> Action </th>
			</tr>

			<?php
			$query = "select * from transaksi";
			$data = mysqli_query($koneksi,$query) or die("Gagal Query:".$query);
			?>

			<?php while($v=mysqli_fetch_array($data)):;
			?>

			<tr height="30" bgcolor="lightblue" align="center">
				<td> <?php echo $v["kd_transaksi"];?> </td>
				<td> <?php echo $v["kd_buku"];?></td>
				<td> <?php echo $v["id_anggota"];?></td>
				<td> <?php echo $v["tgl_transaksi"];?> </td>
				<td><a href="aksi_hapus_transaksi.php?kd_transaksi=<?php echo $v["kd_transaksi"];?>"> Hapus </a></td>
				<td><a href="formupdate_transaksi.php?kd_transaksi=<?php echo $v["kd_transaksi"];?>"> Ubah </a></td>
			</tr>
		<?php endwhile;?>
		</table>
		<a href="input_data_transaksi.php"> Masukkan Data </a><br><br>
		</center>
	</body>
</html>