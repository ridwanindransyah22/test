<?php

	include "koneksi.php";
	$ID_Anggota=$_GET['ID_Anggota'];
	$query="SELECT * FROM anggota where ID_Anggota='$ID_Anggota'";
	$data = mysqli_query($koneksi,$query) or die("Gagal Query:".$query);
	$sql=mysqli_fetch_array($data);

?>

<!DOCTYPE>

<html>
	<head>
		<title> Ubah Data </title>
	</head>
	<body background="bg4.jpg">
	<center>
	<fieldset>
		<legend align="center"><font face="orator std" size="6"><b> Ubah Data Anggota </b></font></legend> <br><br>
		<td width="156" height="256"><p align="center">

	<form action="aksi_update_anggota.php" method="post">
		<table><br><br>
			<tr>
				<td> ID Anggota </td>
				<td> : </td>
				<td> <input type="text" name="ID_Anggota" size="40" placeholder="ID Anggota" readonly value="<?php echo $sql['ID_Anggota'];?>"/> </td>
				<td align="right"> Tanggal Hari Ini </td>
			</tr>

			<tr>
				<td> Nama </td>
				<td> : </td>
				<td> <input type="text" name="Nama" size="40" placeholder="Nama Lengkap" autofocus value="<?php echo $sql['Nama'];?>"/> </td>
				<td align="right"><input type="date" name="tgl_entry" value="<?php echo date("Y-m-d");?>"></td>
			</tr>
			
			<tr>
				<td> Jenis Kelamin </td>
				<td> : </td>
				<td><select name="Jenis_Kelamin">
					<option value="Laki-Laki"> Laki-Laki </option>
					<option value="Perempuan"> Perempuan </option>
					</select>
				</td>
			</tr>

			<tr>
				<td> Alamat </td>
				<td> : </td>
				<td><textarea name="Alamat" size="40" placeholder="Masukkan Alamat"><?php echo $sql['Alamat'];?></textarea></td>
			</tr>

			<tr>
				<td> No. HP </td>
				<td> : </td>
				<td> <input type="text" name="No_HP" size="40" placeholder="Masukkan No. Tlp" maxlength="12" required value="<?php echo $sql['No_HP'];?>"/> </td>
			</tr>

			<tr>
				<td> Email </td>
				<td> : </td>
				<td> <input type="email" name="Email" size="40" placeholder="Email Anda" required value="<?php echo $sql['Email'];?>"/> </td>
			</tr>

			<tr>
				<td> Tanggal Entry </td>
				<td> : </td>
				<td> <input type="date" name="Tgl_Entry" value="<?php echo $sql['Tgl_Entry'];?>"/> </td>
			</tr>
			
			<tr>
				<td colspan="2"></td>
				<td> <input type="submit" name="submit" value="Simpan"></td>
			</tr>
			
		</table><br><br><br><br><br><br>
		<a href="index.php"> Kembali </a>
		</form>
		</fieldset>
		</center>
	</body>
</html>